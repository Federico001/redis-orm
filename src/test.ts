const mockMethod = (filename: string, instance) => jest.mock(filename, () => 
  jest.fn().mockImplementation(() => instance)
