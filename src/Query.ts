import { Entity, OnFulfilled, OnRejected } from "./types"
import { GeoSearchQuery } from "./GeoSearchQuery"
import { Dao } from "./Dao"

/**
 * The Query class translates higher-order operations into
 * lower-level operations on the Model class.
 */
export class Query<T extends Entity> {
	// Todo: the key-weight tuple should be a single array, instead of the 4 we have now
	private keys: string[]
	private sortedKeys: string[]
	private weights: number[]
	private sortedWeights: number[]

	private pageLimit: number
	private pageOffset: number
	private name: string
	private range: [number, number] = [Number.NEGATIVE_INFINITY, Infinity]
	private model: Dao<T>
	private geoSearch?: GeoSearchQuery

	constructor(model: Dao<T>) {
		this.pageLimit = 10
		this.pageOffset = 0
		this.sortedKeys = []
		this.weights = [0]
		this.sortedWeights = []
		this.model = model
		this.name = model.getName()
		this.keys = [this.getSetKey("all", "all")]
	}

	limit = (n: number) => {
		this.pageLimit = n
		return this
	}

	offset = (n: number) => {
		this.pageOffset = n
		return this
	}

	page(n: number) {
		if (n < 1) {
			throw new Error("page number must be 1 or higher")
		}
		this.pageLimit = 10
		this.pageOffset = this.pageLimit * (n - 1)
		return this
	}

	/**
	 * The min (or leftmost boundary) is where results starts. That is the
	 * reason why it defines sorting order.
	 *
	 * Todo: between() should require at least one orderBy() call
	 */
	between(min: number, max: number) {
		this.range = [min, max]
		return this
	}

	orderBy(key: keyof Entity, weight: number = 0, order?: "desc") {
		if (!this.model.sortedIndexes.includes(key)) {
			throw new Error("invalid filter criteria")
		}
		this.sortedKeys.push(this.getSortedSetKey(key))
		this.sortedWeights.push(weight)
		// to achieve desc ordering min > max
		if (order && this.range[0] < this.range[1]) {
			;[[this.range[0], this.range[1]]] = [[this.range[1], this.range[0]]]
		}
		return this
	}

	where(key: string, value: string | string[] | number) {
		if (!this.model.indexes.some((f: string) => f === key)) {
			throw new Error("invalid filter criteria")
		}
		if (Array.isArray(value)) {
			throw new Error("set union is yet unsupported")
		}
		if (typeof value !== "string" && typeof value !== "number") {
			throw new Error(`invalid value query type of ${value}`)
		}
		this.keys.push(this.getSetKey(key, String(value)))
		this.weights.push(0)
		return this
	}

	/*
  whereId(ids: string[]) {

  }
 */

	position(
		latitude: number,
		longitude: number,
		width: number,
		height: number,
		unit: "M" | "KM" | "FT" | "MI" = "KM",
	) {
		this.weights.push(0)
		this.geoSearch = new GeoSearchQuery(
			latitude,
			longitude,
			width,
			height,
			unit,
		)
		return this
	}

	async findOne(id: string) {
		return this.model.findOne(id)
	}

	async _then() {
		let keys = this.keys
		let weights = this.weights
		if (this.sortedKeys.length > 0) {
			keys = keys.concat(this.sortedKeys)
			weights = weights.concat(this.sortedWeights)
		}
		if (this.geoSearch) {
			const glKey = await this.model.geoSearch(
				this.getSortedSetKey("coordinates"),
				this.geoSearch,
			)
			if (glKey === null) {
				throw new Error("bad geosearch")
			}
			keys.push(glKey)
			weights.push(0)
		}
		const fKey = await this.model.intersectIds(keys, weights)
		return this.model.findAll(
			fKey,
			this.pageOffset,
			this.pageLimit,
			this.range,
		)
	}

	then = (
		resolve?: OnFulfilled<T[]> | undefined | null,
		reject?: OnRejected | undefined | null,
	) => {
		const p = this._then()
		if (resolve) {
			p.then(resolve)
		}
		if (reject) {
			p.catch(reject)
		}
		return this
	}

	public catch(callback: Function) {
		callback(null)
		return this
	}

	public finally(callback: Function) {
		callback(null)
		return this
	}

	[Symbol.toStringTag] = () => {
		return "[object Promise]"
	}

	private getSetKey(filterName: string, filterValue: string) {
		return `entity:${this.name}:by-${filterName}:${filterValue}`
	}

	private getSortedSetKey(index: string) {
		return `entity:${this.name}:sorted-by-${index}`
	}
}

/*
class Command {
	args: string[]
	constructor() {}

	exec() {}

	// determines the command the query needs according to the args passed and the redis version
	static factory() {}
}

class ZInterStoreCommand extends Command {
	keyName: string // the key in which the result of the query is stored
}

class ZRangeArgs extends Command {
	constructor() {
		//super('name', args)
		
	}
	this.redis
    	.zrevrangebyscoreAsync(q.getZRangeArgs())
        .then(castArray)

}
*/
