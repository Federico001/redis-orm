import { DistanceUnit } from "./types"

export class GeoSearchQuery {
	latitude: string
	longitude: string
	width: number
	height: number
	unit: "M" | "KM" | "FT" | "MI" = "KM"

	constructor(
		lat: number,
		lon: number,
		w: number,
		h: number,
		unit: DistanceUnit,
	) {
		this.latitude = String(lat)
		this.longitude = String(lon)
		this.width = w
		this.height = h
		this.unit = unit
	}

	toString = () => {
		const lat = this.latitude.slice(0, 7)
		const lon = this.longitude.slice(0, 7)
		return `${lat}-${lon}`
	}
}
