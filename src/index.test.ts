import { createClient } from "redis"
import { Dao } from "./Dao"

describe("", () => {
	it("should create a new entity", async () => {
		type User = {
			id: string
			coordinates?: [number, number]
			gender: string
			city: string
			firstName: string
			height: number
		}

		const client = createClient({ url: "redis://localhost:3336" })
		await client.connect()

		const user = new Dao<User>(
			"user",
			["gender", "city"],
			["height"],
			client,
		)

		const u = await user.persist({
			id: "1",
			gender: "female",
			coordinates: [10, 10],
			firstName: "Laura",
			height: 61,
			city: "Duisburg",
		})
		expect(u).toBe(true)
	})
})
