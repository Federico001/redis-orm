export interface Entity {
	id: string
	coordinates?: [number, number]
}
export interface Coordinates {
	latitude: string
	longitude: string
}

export interface GeoSearchQuery {
	latitude: number
	longitude: number
	width: number
	height: number
	unit: "M" | "KM" | "FT" | "MI"
}

export type DistanceUnit = "M" | "KM" | "FT" | "MI"

export type KeysMatching<T extends object, V> = {
	[K in keyof T]-?: T[K] extends V ? K : never
}

export interface RedisClient<T = any> {
	set: (key: string, value: string) => Promise<number>
	del: (key: string) => Promise<number>
	sadd: (key: string, id: string) => Promise<number>
	srem: (key: string, id: string) => Promise<number>
	sinter: (sets: string[]) => Promise<number>
	mget: (keys: string[]) => Promise<T>
	zrange: (keys: string[]) => Promise<string[]>
	zadd: (keys: string[]) => Promise<void>
	zinterstore: (keys: string[]) => Promise<void>
	exists: (key: string) => Promise<-1 | 0>
	expire: (key: string) => Promise<-1 | 0>
}

export interface Thenable {
	then(callback: Function): Thenable
	catch(callback: Function): Thenable
	finally(callback: Function): Thenable
}

export type OnFulfilled<TResult1> = (
	value: TResult1,
) => TResult1 | PromiseLike<TResult1>

export type OnRejected = <TResult2 = never>(
	reason: any,
) => TResult2 | PromiseLike<TResult2>
