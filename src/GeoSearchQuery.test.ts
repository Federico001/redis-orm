import { GeoSearchQuery } from "./GeoSearchQuery"

describe("GeoSearchQuery", () => {
	it("should not allow latitudes bigger than 90", () => {
		expect(new GeoSearchQuery(100, 10, 100, 10, "KM")).toThrowError()
	})
})
