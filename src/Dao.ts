import { KeysMatching, Entity } from "./types"
import { GeoSearchQuery } from "./GeoSearchQuery"
import { Query } from "./Query"

type NumberKeys<T extends object> = keyof KeysMatching<T, number>

type ExtractString<T> = T extends string ? T : never

const modelName = Symbol("name")

export class Dao<T extends Entity = any> {
	readonly [modelName]: string
	// The 'readonly' prevents inconsistencies between queries and storage
	// if the client code can add indexes after some entitites have been persisted, then
	// the queries would allow to search by inexistent indexes. This doesn't prevent errors
	// for different instantiations of the client code, which might include different
	// options each time.
	readonly indexes: Array<ExtractString<keyof T>>
	// NumberKeys makes sure that the properties selected to be sorted intexes
	// are of number type
	readonly sortedIndexes: Array<NumberKeys<T>>
	redis: any //RedisClient

	constructor(
		name: string,
		indexes: Array<ExtractString<keyof T>>,
		sorted: Array<NumberKeys<T>>,
		connection: Promise<any> | any,
	) {
		this[modelName] = name
		this.indexes = indexes // model.getIndexes()
		this.sortedIndexes = sorted // model.getSortedIndexes()
		this.redis = connection
	}

	getName() {
		return this[modelName]
	}

	async persist(entity: T) {
		await this.redis.set(
			this.getEntityKey(entity.id),
			JSON.stringify(entity),
		)
		this.redis.sAdd(this.getSetKey("all", "all"), String(entity.id))

		for (let index of this.indexes) {
			if (Array.isArray(entity[index])) {
				for (let value of entity[index] as unknown as Array<any>) {
					await this.redis.sAdd(
						this.getSetKey(index, String(value)),
						String(entity.id),
					)
				}
				continue
			}
			await this.redis.sAdd(
				this.getSetKey(index, String(entity[index])),
				String(entity.id),
			)
		}
		for (let index of this.sortedIndexes) {
			// check if index is defined
			if (!entity.hasOwnProperty(index)) {
				throw new Error(`a score shoud be defined in prop ${index}`)
			}
			if (Number.isNaN(Number(entity[index]))) {
				throw new Error(`sorted prop ${index} should be a valid number`)
			}
			await this.redis.zAdd(this.getSortedSetKey(String(index)), {
				score: entity[index],
				value: String(entity.id),
			})
		}
		if (Array.isArray(entity.coordinates)) {
			await this.redis.geoAdd(this.getSortedSetKey("coordinates"), {
				latitude: entity.coordinates[0],
				longitude: entity.coordinates[1],
				member: String(entity.id),
			})
		}
	}

	async destroy(entity: T) {
		//await this.redis.del(this.getEntityKey(entity.id))

		for (let index of this.indexes) {
			Promise.all(
				[]
					.concat(entity[index] as unknown as any)
					.map((val: string | number) =>
						this.getSetKey(index, String(val)),
					)
					.map((key) => this.redis.sRem(key, String(entity.id))),
			)
		}
	}

	/**
	 * Performs a geoSearch and stores the result in a sorted set.
	 *
	 */
	async geoSearch(source: string, search: GeoSearchQuery) {
		const cacheKey = `${source}:${search.toString()}`
		const from = {
			latitude: search.latitude,
			longitude: search.longitude,
		}
		const by = { width: search.width, height: search.height, unit: "km" }
		const opts = { STOREDIST: true }
		await this.redis.geoSearchStore(cacheKey, source, from, by, opts)
		return cacheKey
	}

	/**
	 * Intersects a list of sorted and unsorted sets using their keys as input.
	 * @returns the name of the key that holds all the intersected ids.
	 */
	intersectIds = async (keys: string[], weights?: number[]) => {
		const WEIGHTS = weights == void 0 ? keys.map((_) => 0) : weights
		const cacheKey = this.getCompositeCacheKey(keys)
		if ((await this.redis.exists(cacheKey)) === 0) {
			await this.redis.zInterStore(cacheKey, keys, { WEIGHTS })
			await this.redis.expire(cacheKey, 60)
		}
		return cacheKey
	}

	private formatRange(range: [number, number]) {
		return range.map((r) => {
			if (r === Number.NEGATIVE_INFINITY) return "-inf"
			if (r === Infinity) return "+inf"
			return r
		})
	}

	findOne = async (id: string): Promise<T> => {
		const serialized: string = await this.redis.get(this.getEntityKey(id))
		return JSON.parse(serialized)
	}

	findAll = async (
		key: string,
		offset: number,
		limit: number,
		range: [number, number],
	): Promise<T[]> => {
		const [left, right] = range
		const [min, max] = this.formatRange(range)
		const ids = await this.redis.zRange(key, String(min), String(max), {
			BY: "SCORE",
			LIMIT: { offset: String(offset), count: String(limit) },
			REV: left > right,
		})
		if (ids.length === 0) {
			return []
		}
		const serialized: string[] = await this.redis.mGet(
			ids.map(this.getEntityKey),
		)
		return serialized.map((s) => JSON.parse(s))
	}

	query() {
		return new Query(this)
	}

	// The composite key is used to store complex queries, e.g. "give me all entities that
	// are in region R, for sport S, ordered by date"
	private getCompositeCacheKey(keys: string[]) {
		// The sorting prevents that two different keys are created for groups of filters with
		// different order, i.e., one key for region-austria:sport-soccer and other for
		// sport-soccer:region-austria.
		const ks = keys
			.sort((k0, k1) => k0.localeCompare(k1))
			.reduce((p, c) => `${p}:${c}`, "")
		return `entity:${this[modelName]}:query${ks}`
	}

	private getEntityKey = (pk: string) => {
		return `entity:${this[modelName]}:${pk}`
	}

	private getSetKey(filterName: string, filterValue: string) {
		return `entity:${this[modelName]}:by-${filterName}:${filterValue}`
	}

	private getSortedSetKey(index: string) {
		return `entity:${this[modelName]}:sorted-by-${index}`
	}
}
