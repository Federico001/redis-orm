import { GeoSearchQuery } from "./GeoSearchQuery"
import { Query, IModel } from "./Query"
import { Dao } from "./Dao"
import { mocked } from "ts-jest/utils"

type FindAll = <T>(
	_key: string,
	_offset: number,
	_limit: number,
	_range: [number | "-inf", number | "+inf"],
) => Promise<T[]>

const mockMethod = (filename: string, instance: any) =>
	jest.mock(filename, () => ({
		Dao: jest.fn().mockImplementation(() => instance),
	}))

const findAll = jest.fn()

const instance = {
	indexes: ["bar", "baz"],
	getName: () => "foo",
	geoSearch: (_: string, __: GeoSearchQuery) => Promise.resolve("foo"),
	intersectIds: (_keys: string[], _weights?: number[]) =>
		Promise.resolve("id"),
	findAll: () => [],
}

mockMethod("./Dao", instance)

it("should return arguments for a valid redis command", async () => {
	const mockedDao = mocked(Dao, true)
	const q = new Query(mockedDao).where("bar", "42")
	expect(await q).toBe("foo")
})

it("should chain a number of promises", async () => {
	const mockedDao = mocked(Dao, true)
	const q = await new Query(mockedDao).then((a) => a).then((b) => b)
	expect(q).toBe(true)
})
