# Introduction

A ORM for Redis to store and query denormalized data.

# Use

## Persisting entities

Entities are sorted/stored based in qualitative values like city or
country, (which are subject to filtering) and qualitative values like
birth date, income, height or weight (which are subject to sorting).

```javascript
import { createClient } from "redis"
import { Model } from "./Model"

type User = {
    id: string
    coordinates?: [number, number]
    gender: string
    city: string
    firstName: string
    height: number
}

const fn = async () => {
	const client = createClient({ url: "redis://localhost:3336" })
	await client.connect()
    const qb = new QueryBuilder(client)

    /*
    const user = gq.createModel({
      name: "user",
      sortedAttributes: [ "gender", "foo" ],
      unsortedAttributes: [ "height" ],
    })
    */

	const user = new Orm<User>("user", ["gender", "city"], ["height"], client)

	await user.persist({
		id: "1",
		gender: "female",
		coordinates: [10, 10],
		firstName: "Laura",
		height: 61,
		city: "Duisburg",
	})

	await user.persist({
		id: "2",
		gender: "female",
		firstName: "Octavia",
		height: 51,
		city: "Duisburg",
	})

	await user.persist({
		id: "3",
		gender: "male",
		firstName: "Paul",
		height: 100,
		city: "Duisburg",
	})

}
```


## Querying entities

```javascript
const q = user
    .query()
    .where("gender", "female")
    .where("city", "Duisburg")
    .inArea(10, 10, 10, 10)
    .orderBy("height")
    .page(1)

const a = await q
console.log(a)
/*
  [
    {
      id: '1',
      gender: 'female',
      coordinates: [ 10, 10 ],
      firstName: 'Laura',
      height: 61,
      city: 'Duisburg'
    }
  ]
 */
```

## Contributing

### Todo

0. Make then() work properly on successive then()s (now it only works
   on first call)
1. Transform functions that span multiple queries into a transaction.
2. Move key-name-building logic into the Query class.
3. Prevent name-collision in the naming schema of the keys.
4. Prevent duplicated filters
5. Whitelist filters with the Model
6. Prevent that all:all to be always called. Remove as default when
   first query is made.


## Error handling

Every time a query is attempted against an inexistent filter, an Error
is thrown.
